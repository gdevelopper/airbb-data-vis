class View {
  /**
   * 
   * @param {données + method de transformation} data 
   * @param {Nom de la class html} htmlId 
   */
  constructor(data, htmlId) {
    this.data = data;
    this.htmlId = htmlId;
  }
  /**
   * 
   */
  render() {
    throw new Error("You have to implement this abstract method");
  }
  /**
   * 
   */
  update() {
    throw new Error("you have to implement this abstract method");
  }
}


class MapView extends View {
  
  constructor(data, htmlId,radar) {
    super(data, htmlId)
    this.level = "New York";
    this.numeric_metric = "price";
    this.min = 0;
    this.max = 10000;
    this.parent = undefined;
    this.groupe = undefined;
    this.radar = radar;
    this.scl =[[0, 'rgb(150,0,90)'],
              [0.125, 'rgb(0, 0, 200)'],
              [0.25,'rgb(0, 25, 255)'],
              [0.375,'rgb(0, 152, 255)'],
              [0.5,'rgb(44, 255, 150)'],
              [0.625,'rgb(151, 255, 0)'],
              [0.75,'rgb(255, 234, 0)'],
              [0.875,'rgb(255, 111, 0)'],
              [1,'rgb(255, 0, 0)']
    ];
    this.data.ScatterMapData(this.level,this.groupe,this.parent,this.numeric_metric,this.min,this.max).then((obj) => {
      this.zoom = obj.zoom;
      this.center = obj.center
      this.longitude = obj.longitude;
      this.latitude = obj.latitude;
      this.colors = obj.colors;
      this.texts = obj.texts;
      let html = this.htmlId
      let radar = this.radar
      this.render(obj.max).then(()=>{
        document.getElementById(html).on("plotly_click",(data)=>{
          let r = /\d+$/
          let host1 = r.exec(data.points[0].text)[0]
           radar.update(parseInt(host1))
           $("#radar-plot-container").fadeToggle(1000);
        });
      })
    })

  }
  render() {
    let donnees = [{
      type: 'scattermapbox',
      lat: this.latitude,
      lon: this.longitude,
      mode: 'markers',
      text : this.texts,
      marker: {
        color: this.colors,
        colorscale: this.scl,
        cmin: this.min,
        cmax: 1*this.max,
        reversescale: true,
        opacity: 0.5,
        size: 8,
        colorbar:{
          thickness: 10,
          outlinecolor: 'rgba(68,68,68,0)',
          ticks: 'inside',
          ticklen: 3,
          showticksuffix: 'all',
          ticksuffix: " $/night",
          dtick: 0.125*this.max,

          xanchor:"right",
          x:1,
          xpad:20,
          yanchor:"bottom",
          y:0,
          ypad:75,
        }
      }
    }]

    let layout = {
      dragmode: 'zoom',
      mapbox: {
        center:this.center,
        zoom: this.zoom
      },
      margin: {
        r: 0,
        t: 0,
        b: 0,
        l: 0,
        pad: 0
      },
      showlegend: false
    };
    Plotly.setPlotConfig({
      mapboxAccessToken: 'pk.eyJ1IjoiZ2hpbGVzLWtpbmctOTQiLCJhIjoiY2s0bGI4YTRqMjgydDNubzJnOG04NXNjNCJ9.iKi2HZqD_EUARDpkSWt7Mg'
    })
    return Plotly.plot(this.htmlId, donnees, layout, {displayModeBar: false /*,responsive:true*/});
  }
  
  update(level,groupe,parent,numeric_metric,min,max) {

      this.groupe = groupe != undefined ? groupe : this.groupe;
      this.level = level != undefined ? level : this.level;
      this.parent = parent != undefined ? parent :this.parent;
      this.max = max != undefined ? max : this.max;
      this.min = min != undefined ? min : this.min;
      this.numeric_metric = numeric_metric != undefined ? numeric_metric : this.numeric_metric
      let labels =  {
            "price" : " $ / Night",
            "minimum_nights" : " Nights",
            "number_of_reviews": " Reviews",
            "reviews_per_month": " Reviews/Month",
            "calculated_host_listings_count": " Hosts",
            "availability_365":  " Neight/365" ,
      }
      
      this.data.ScatterMapData(this.level,this.groupe,this.parent,this.numeric_metric,this.min,this.max).then((obj)=>{
        
        this.zoom = obj.zoom;
        this.center = obj.center;
        this.longitude = obj.longitude;
        this.latitude = obj.latitude;
        this.colors = obj.colors != undefined ? obj.colors : this.colors;
        this.texts = obj.texts

        Plotly.animate(this.htmlId, 
          {
          data: [{
            lat: this.latitude,
            lon: this.longitude,
            mode: 'markers',
            text : this.texts,
            marker: {
              color: this.colors,
              cmin: this.min,
              cmax: this.max,
              colorbar:{
                ticksuffix: labels[numeric_metric],
                dtick: 0.125*this.max
              }
            }
          }],
          layout: {
            mapbox: {
              bearing: 0,
              center: this.center,
              pitch: 0,
              zoom: this.zoom
            }
          }
          }, {
          transition: {
            duration: 500,
            easing: 'cubic-in-out'
          },
          frame: {
            duration: 500
          }
        })
      })  
  }
}

class SunBurstView extends View {
  /**
   *  
   */
  constructor(data, htmlId, map,boxplot) {
    super(data, htmlId);
    this.map = map
    this.boxplot = boxplot;
    this.data.SunBurstInit().then((obj) => {
      let l = Object.keys(obj);
      let labels = l;
      
      let parents = ["","New York", "New York", "New York", "New York", "New York"];
      for(let i =0 ;i < 5; i++){
        obj[l[i]].forEach(e2 => {
          labels.push(e2);
          parents.push(l[i]);
        })
      }
      labels.unshift("New York")
      this.labels = labels
      this.parents = parents
      let values  = []
      labels.forEach(e=>{
        values.push(obj["count"][e])
      })
      this.values = values;
      let p = this.render()
      // mise en place des écouteurs d'événement 
      p.then(() => {
        // initialiser les ecouter d'événement 
        let map = this.map;
        let boxplot = this.boxplot
        document.getElementById(this.htmlId).on("plotly_sunburstclick", function (data) {
          let groupe = data.points[0].label
          let parent = data.points[0].parent 
          if( groupe != "Manhattan" && groupe != "Brooklyn" && groupe != "Queens" && groupe != "Bronx" && groupe != "Staten Island" && groupe != "New York" ){
            map.update("neighbourhood",groupe,parent)
            $("#path").html("<span class='level'>New York </span><span class='level'> <i class='right'></i> "+parent+"  </span><span class='level'> <i class='right'></i> "+groupe+"</span>")
          }else if( map.level != "neighbourhood_group" && ( groupe == "Manhattan" || groupe == "Brooklyn" || groupe == "Queens" || groupe == "Bronx" || groupe == "Staten Island") ){
            
            map.update("neighbourhood_group",groupe,parent)
            boxplot.update(groupe)
            $("#path").html("<span class='level'>New York  </span><span class='level'> <i class='right'></i> "+groupe+"</span>");
          }else{
            map.update("New York")
            boxplot.update("New York")
            $("#path").html("<span class='level'>New York</span>")
          }
        });
      })
    })

  }
  /**
   * 
   */
  render() {
    // je vais l'implémenter

    var data = [{
      type: "sunburst",
      labels: this.labels,
      parents: this.parents,
      values : this.values,
      //text : this.values,
      hovertemplate: '<b>%{label}</b>'+
                     '<br><b>%{value}</b> hosts<br>'
    }]
    var layout = {
      margin: {
        t: 0,
        l: 0,
        r: 0,
        b: 0
      },
      width: 400,
      height: 400,
      paper_bgcolor: "rgba(0,0,0,0)",
      plot_bgcolor: "rgba(0,0,0,0)"
    }
    return Plotly.plot(this.htmlId, data, layout, {
      responsive: true,
      displayModeBar: false
    })
  }
  /**
   * cette méthode ne fais rien ou pas implementer 
   */
  update() {
    // je vais l'implémenter
    console.log("cette méthode n'est pas implémenté car le sun burst n'interagie pas avec les autres vues")
  }
}


class BoxPlotView extends View{

  constructor(data, htmlId){
    super(data,htmlId);
    this.level = "New York";
    this.numeric_metric = "price";
    this.data.BoxPlotInit().then((obj=>{
      this.agg = obj  
      this.data.BoxPlotData(this.level,this.agg,this.numeric_metric).then((_data)=>{
        this._data  = _data
        let p = this.render()
        
        p.then(()=>{
          // ajouter le écouter d'événément dans 
        })
       });
    })
    );
    
  
  }

  render(){
        let y = Object.keys(this._data);
        let data_final = [];  
        let colors = ["rgba(44, 160, 44, 0.5)","rgba(148,103,189, 0.5)","rgba(31, 119, 180, 0.5)","rgba(214, 39, 40, 0.5)","rgba(255,127,14, 0.5)"]
        let titles = {"price":"Prix",
                      "minimum_nights":"Nombre de nuits minimales",
                      "number_of_reviews":"Nombre de visites",
                      "reviews_per_month":"Nombre de visite par semaine",
                      "calculated_host_listings_count":"Nombre calculé de client par hote",
                      "availability_365":"Nombre de nuits valables par an" 
                    }
        //Create Traces
        for( let i = 0; i < y.length;  i++ ){
            let result = {
                type: 'box',
                y: this._data [y[i]],
                name: y[i],
                boxmean: true,
                boxpoints: false,
                fillcolor: y.length == 5 ? colors[i] : "cls" ,
                line: {
                    color: y.length == 5 ? colors[i] : "cls" ,
                    width: 1
                }
            };
            data_final.push(result);
        };
        //Format the layout
        let layout = {
            width: window.innerWidth*0.8,
            height: window.innerHeight*0.6,
            title: titles[this.numeric_metric],
            yaxis: {
                autorange: true,
                showgrid: true,
                zeroline: true,
                dtick: 5,
                gridcolor: 'rgb(243, 243, 243)',
                gridwidth: 1,
                zerolinecolor: '#369EF5',
                zerolinewidth: 2
            },
            margin: {
                l: 1,
                r: 0,
                b: 120,
                t: 100
            },
            paper_bgcolor: 'rgb(243, 243, 243)',
            plot_bgcolor: 'rgb(243, 243, 243)',
            showlegend: true
        };
        return Plotly.newPlot(this.htmlId, data_final, layout);
  }
/**
 * 
 * @param {*} level 
 * @param {*} numeric_metric 
 * 
 * input : va avoir accée boxplot => mets à jour la numéric_metric  
 * sunburst : va avoir accée boxplot => mettre à jour le level 
 * 
 * ces deux mise à jours vont être faitent avec la fonction update de l'objet MAP
 */
  update(level,numeric_metric){
    this.level = (level != undefined ? level :this.level);
    this.numeric_metric =(numeric_metric != undefined ? numeric_metric : this.numeric_metric);

    // à ce niveau là je recalcule le jeu de données pour  
    this.data.BoxPlotData(this.level,this.agg,this.numeric_metric).then((obj)=>{
      this._data = obj
      let y = Object.keys(this._data);
      let colors = ["rgba(44, 160, 44,0.5)","rgba(148,103,189,0.5)","rgba(31, 119, 180,0.5)","rgba(214, 39, 40,0.5)","rgba(255,127,14,0.5)"]
      let data_final = []; 
      let titles = {"price":"Price : price in dollars ($)",
        "minimum_nights":"Minimum Nights : Amount of Nights Minimum",
        "number_of_reviews":"Number Of Reviews (web site visits) ",
        "reviews_per_month":"Reviews Per Month : Number of Reviews Per Month",
        "calculated_host_listings_count":"Calculated Host Listings Count : Amount of Listing Per Host",
        "availability_365":"Availability 365 : number of days when listing is available for booking" }  
      //Create Traces
      for( let i = 0; i < y.length;  i++ ){
          let result = {
              type: 'box',
              y: this._data [y[i]],
              name: y[i],
              boxmean: true,
              boxpoints: false,
              fillcolor: y.length == 5 ? colors[i] : "cls" ,
              line: {
                color: y.length == 5 ? colors[i] : "cls" ,
                width: 1
              }
          };
          data_final.push(result);
      };
      //Format the layout
      
      let layout = {
          width: window.innerWidth*0.8,
          height: window.innerHeight*0.6,
          title: titles[this.numeric_metric],
          yaxis: {
              autorange: true,
              showgrid: true,
              zeroline: true,
              dtick: 5,
              gridcolor: 'rgb(243, 243, 243)',
              gridwidth: 1,
              zerolinecolor: '#369EF5',
              zerolinewidth: 2
          },
          margin: {
              l: 1,
              r: 0,
              b: 120,
              t: 100
          },
          paper_bgcolor: 'rgb(243, 243, 243)',
          plot_bgcolor: 'rgb(243, 243, 243)',
          showlegend: true
      };
      
      Plotly.react(this.htmlId,data_final,layout)
    })
  }
}

class ScatterPolarView extends View{
  constructor(data,htmlId){
    super(data,htmlId);
    this.host1 = undefined;
    this.host2 = undefined;
    this.render().then(()=>{
      // ici je peux ajouter des ecouteurs d'événements 
    })
  }

  render(){
      let data = []

      let layout = {
        polar: {
            radialaxis: {
            visible: false,
            range: [0, 1]
            }
        }
      }

      return Plotly.plot(this.htmlId, data, layout)
  }

  update(host1,host2){
    this.host1 = host1 != undefined ? host1 : this.host1;
    this.host2 = host2 != undefined ? host2 : this.host2;
    this.data.ScatterPolarData(this.host1,this.host2).then(hosts => {
      let data  = []
      if(hosts.host1 != undefined){
      // mise en place du tableau détailler des informations
      let info = "<table style='"+
      "padding-left: 5px;"+
      "margin-bottom: 0px;"+
      "height: 100%;border:1'>"+
      "<tr>"+
        "<td><i class='fa fa-passport'></i></td>"+
        "<td>Id</td>"+
        "<td> "+hosts.host1.id[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-id-badge'></i></td>"+
        "<td>Host Id </td>"+
        "<td>"+hosts.host1.host_id[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-id-card'></i></td>"+
        "<td>Host Name</td>"+
        "<td>"+hosts.host1.host_name[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-signature'></i></td>"+
        "<td> Name</td>"+
        "<td>"+hosts.host1.name[0]+"</td>"+
      "</tr>"+
     "<tr>"+
       "<td><i class='fa fa-clock'></i></td>"+
        "<td>Last Review</td>"+
        "<td>"+hosts.host1.last_review[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-bed'></i></td>"+
        "<td>Room Type</td>"+
        "<td> "+hosts.host1.room_type[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-map-marker-alt'></i></td>"+
        "<td>Latitude</td>"+
        "<td> "+hosts.host1.latitude[0]+"°</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-map-marker-alt'></i></td>"+
        "<td>Longitude</td>"+
        "<td>"+hosts.host1.longitude[0]+"°</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-building'></i></td>"+
        "<td>Neighbourhood</td>"+
        "<td>"+hosts.host1.neighbourhood[0]+"</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fas fa-city'></i></td>"+
        "<td>Neighbourhood Group</td>"+
        "<td>"+hosts.host1.neighbourhood_group[0]+"</td>"+
      "</tr>"+

      "<tr>"+
        "<td><i class='fa fa-moon'></i> </td>"+
        "<td>Minimum Nights</td>"+
        "<td>"+hosts.host1.minimum_nights[0]+" neights</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-door-open'></i></td>"+
        "<td> Availability 365 </td>"+
        "<td> "+hosts.host1.availability_365[0]+" neights</td>"+
      "</tr>"+
      "<tr>"+
        "<td><i class='fa fa-address-book'></i></td>"+
        "<td>Calculated Host Listings Count</td>"+
        "<td> "+hosts.host1.calculated_host_listings_count[0]+"</td>"+   
      "</tr>"+
      "<tr>"+
          "<td><i class='fa fa-search'></i></td>"+
          "<td>Number of Reviews</td>"+
          "<td>"+hosts.host1.number_of_reviews[0]+" Reviews</td>"+
      "</tr>"+
      "<tr>"+
          "<td><i class='fa fa-dollar-sign'></i></td>"+
          "<td>Price</td>"+
          "<td>"+hosts.host1.price[0]+" $</td>"+
      "</tr>"+
      "<tr>"+
          "<td><i class='fa fa-history'></i></td>"+
          "<td>Reviews Per Month</td>"+
          "<td>"+hosts.host1.reviews_per_month[0]+"</td>"+ 
      "</tr>"+
      "</table>";
        $("#host-information").html(info);
        console.log(info)
      // mise ne place du graphe   
      let h1 = {
        type: 'scatterpolar',
        r: hosts.host1.draw,
        theta: ["price $","minimum nights","number of reviews","reviews per month","calculated host listings count","availability 365"],
        fill: 'toself',
        name: hosts.host1.name[0],
        text : [hosts.host1.price[0], hosts.host1.minimum_nights[0], hosts.host1.number_of_reviews[0], hosts.host1.reviews_per_month[0], hosts.host1.calculated_host_listings_count[0], hosts.host1.availability_365[0]],
        hovertemplate: '<i>%{theta}</i>: <b>%{text}</b>'
      };
      data.push(h1)
    }
    if(hosts.host2 != undefined){
      let h2 = {
        type: 'scatterpolar',
        r : hosts.host2.draw,
        theta : ["price $","minimum nights","number of reviews","reviews per month","calculated host listings count","availability 365"],
        fill : 'toself',
        name : hosts.host2.name[0],
        text : [hosts.host2.price[0], hosts.host2.minimum_nights[0], hosts.host2.number_of_reviews[0], hosts.host2.reviews_per_month[0], hosts.host2.calculated_host_listings_count[0], hosts.host2.availability_365[0]],
        hovertemplate: '<i>%{theta}</i>: <b>%{text}</b>'
      };
      data.push(h2)
    }
    let layout = {
      width: window.innerWidth* 0.666  ,
      height: window.innerHeight*(0.64),
      polar: {
          radialaxis: {
          visible: false,
          range: [0, 1]
          }
      }
    }
    Plotly.react(this.htmlId,data,layout)
    })
  }
}

class Data {
  /**
   * ici il va recevoir le data frame 
   */
  constructor(df) {
    this.data = df
  }

  ScatterMapData(level, groupe,parent,numeric_metric,min,max) {

    return new Promise((resolve, reject) => {
      try {
        
        let centers = {
          "Bronx": {lat: 40.8370495, lon: -73.8654295},
          "Queens": {lat: 40.742054, lon: -73.769417},
          "Brooklyn": {lon: -73.9441579, lat: 40.6781784},
          "Staten Island": {lat: 40.6039, lon: -74.1472},
          "Manhattan" : {lat: 40.7896239, lon: -73.9598939}
        }
        let longitude = [];
        let latitude = [];
        let colors =  [];
        let texts =  [];
        let ids  = []
        let zoom = undefined;
        let center = undefined;
        let res = undefined;
        //let max = this.data.stat.max(numeric_metric); 
        if (level == "New York" ) {
          res = this.data.filter(row => row.get(numeric_metric) >= min && row.get(numeric_metric)<= max).select("longitude", "latitude", numeric_metric,"name","host_name","room_type","last_review","id").toArray();
          res.forEach(element => {
            if (element[0] != undefined && element[1] != undefined ) {
              longitude.push(element[0]);
              latitude.push(element[1]);
              colors.push(element[2]);
              texts.push("<b>Offer Name</b> : "+element[3] +"</br><b>Host Name</b> : "+element[4] +"</br><b>Room Type</b> : "+ element[5]+"</br><b>Last Review</b> : "+element[6]+"</br><b>Id : </b>"+element[7])
              ids.push(element[7])
            }
          })
          zoom = 9.5
          center = {"lat": 40.70, "lon": -73.98};
        } else if (level != "New York" && groupe != undefined) {
          res = this.data.filter(row => row.get(numeric_metric) >= min && row.get(numeric_metric) <= max && row.get(level) === groupe).select("latitude", "longitude", numeric_metric,"name","host_name","room_type","last_review","id").toArray()
          res.forEach(element => {
            if (element[0] != undefined && element[1] != undefined) {
              longitude.push(element[1]);
              latitude.push(element[0]);
              colors.push(element[2]);
              texts.push("<b>Offer Name</b> : "+element[3] +"</br><b>Host Name</b> : "+element[4] +"</br><b>Room Type</b> : "+ element[5]+"</br><b>Last Review</b> : "+element[6]+"</br><b>Id : </b>"+element[7])
              ids.push(element[7])
            }
          })
          level == "neighbourhood_group" ? zoom = 11 : zoom = 12
          center = {
            lat: level == "neighbourhood_group"? centers[groupe].lat:centers[parent].lat,
            lon: level == "neighbourhood_group"? centers[groupe].lon:centers[parent].lon
          }
        }

        resolve({"longitude": longitude,"latitude": latitude,"colors": colors,"zoom": zoom,"center": center,"max":max,"texts":texts});

      } catch (e) {
        reject(e);
      }
    });
  }

  SunBurstInit() {
    return new Promise((success, _fail) => {
      // récupèrer les neighbourhood_group
      let ng = this.data.distinct("neighbourhood_group").toArray()
      ng = ng.map(e => e[0]);
      let obj = {}
      ng.forEach(e => obj[e] = this.data.filter(row => row.get("neighbourhood_group") == e).distinct("neighbourhood").toArray().map(e => e[0]));
      let count  = {}
      this.data.groupBy("neighbourhood").aggregate(group => group.count()).rename('aggregation', 'groupCount').toArray().forEach(e=>{count[e[0]] = e[1]})
      this.data.groupBy("neighbourhood_group").aggregate(group => group.count()).rename('aggregation', 'groupCount').toArray().forEach(e=>{count[e[0]] = e[1]})
      count["New York"] = this.data.count()
      obj["count"] = count;
      success(obj)
    })
  }

  BoxPlotInit(){
    return new Promise((success, _fail) => {
      let res = {}
      let ng = this.data.distinct("neighbourhood_group").toArray().map(e=>e[0])
      ng.pop()
      ng.forEach(e=>{
        res[e] = this.data.filter(row=> row.get("neighbourhood_group") == e).distinct("neighbourhood").toArray().map(e=>e[0])
      })
      success(res);
    })
  }

  BoxPlotData(level,agg,numeric_metric){
    return new Promise((success,_reject)=>{
      let data  = {}
      if(level == "New York"){
        /*** neighbourhood_group */
        Object.keys(agg).forEach(e=>{
          data[e] = this.data.filter(row=> row.get("neighbourhood_group") == e).select(numeric_metric).toArray().map(e=>e[0])
        })
      }else{
        /*** neighbourhood_group */
        agg[level].forEach(e=>{
          data[e] = this.data.filter(row=> row.get("neighbourhood") == e).select(numeric_metric).toArray().map(e=>e[0])
        })
      }
      success(data)
    })
  }

  ScatterPolarData(host1,host2){
    return new Promise((success,_reject)=>{
      if( host1 != undefined ){
        host1 = this.data.filter(row => row.get("id") == host1).toDict()
        host1["draw"] = this.normalize(host1.price[0],
                                       host1.minimum_nights[0],
                                       host1.number_of_reviews[0],
                                       host1.reviews_per_month[0],
                                       host1.calculated_host_listings_count[0],
                                       host1.availability_365[0])
      }
        
      if( host2 != undefined ){
        host2 = this.data.filter(row => row.get("id") == host2).toDict()
        host2["draw"] = this.normalize(host2.price[0],
          host2.minimum_nights[0],
          host2.number_of_reviews[0],
          host2.reviews_per_month[0],
          host2.calculated_host_listings_count[0],
          host2.availability_365[0])
      }

      success({"host1":host1 ,"host2": host2});
    })
  }

  normalize(price,minimum_neight,nbr_review,review_month,host_list,availability){
    let p = (price - 0)/(10000-0);
    let m = (minimum_neight - 1) / (1250 - 1);
    let n = (nbr_review - 0) / (629-0);
    let r = (review_month - 0.01) / (58.5 - 0.01);
    let h = (host_list - 1) / (327 - 1); 
    let a = (availability - 0) / (365 - 0);
    return [p,m,n,r,h,a];
  }

  initHostSelect(){
    return new Promise((success,reject)=>{
     let select =  this.data.select("id","name")
               .toArray()
              .reduce((acc,prev) => acc + "<option value='"+prev[0]+"'>"+prev[1]+"</option>","")
      success(select);
    })
  }
}